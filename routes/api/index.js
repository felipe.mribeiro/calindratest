const express = require('express');
const router = express.Router();

const { Client } = require('@googlemaps/google-maps-services-js')

    function getLocation(streetAddress){
    const geocodingClient = new Client({});
    let params = {
            address: streetAddress,
            components: 'country:BR',
            key: process.env.GOOGLE_MAPS_API_KEY
        }; 
    
        console.log('retrieving lat, lng for ' + streetAddress);
        geocodingClient.geocode({
            params:params
        })
        .then((response)=>{
            console.log('status: ' + response.data.status);
            console.log(response.data.results[0].geometry.location.lat);
            console.log(response.data.results[0].geometry.location.lng);
            return response;
        })
        .catch((error)=>{
            console.log('error retrieving geocoded results');
        });
    }

    getLocation()
    async function mapsGet(req) {
        const result = await getLocation(req.query.address)
        return result
    }
module.exports = {
    getLocation,
    mapsGet
}
